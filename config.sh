#!/bin/bash
#

readoptions() {

local mode;


while [[ $# > 0 ]]
do

	local key="$1"
	shift

	case $key in
	--init)
	    mode="init"
            echo $mode
	    return 0
	    ;;
             *)
            echo "usage: $0 [--init]"
            return 1
            ;;
esac
done

echo "usage: $0 [--init]"

return 1
}


init() {


        if (( $# < 1 ))
        then 

                local basedir=`dirname $0`/../;
                local easybashgui=( `find $basedir -iname "easybashgui" |  xargs -I {} file {} | grep -v directory| sed -e 's/\://g'` );
                easybashgui=${easybashgui[0]}

                export SHELL_LIBRARY_PATH=`dirname $easybashgui`/../lib
                source $easybashgui;
        else
                export SHELL_LIBRARY_PATH=$1/lib
                source $1/easybashgui;
        fi

}




configurepring() {

[[ $# == 1 ]] && [[ "$1" != "init" ]]  && return 1;

local basedir=`dirname $0`/;
local easybashgui=( `find $basedir -iname "easybashgui" |  xargs -I {} file {} | grep -v directory| sed -e 's/\://g'` );
easybashgui=${easybashgui[0]}


export SHELL_LIBRARY_PATH=`dirname $easybashgui`/../lib
source $easybashgui;


return 0;
}




# the main program starts here

regime=`readoptions "$@"`
[[ $? > 0  ]] && echo "$regime" && exit $?
configurepring $regime

