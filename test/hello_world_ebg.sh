#!/usr/bin/env bash


init() {


	if (( $# < 1 ))
	then 

		local basedir=`dirname $0`/../;
		local easybashgui=( `find $basedir -iname "easybashgui" |  xargs -I {} file {} | grep -v directory| sed -e 's/\://g'` );
		easybashgui=${easybashgui[0]}

		export SHELL_LIBRARY_PATH=`dirname $easybashgui`/../lib
		source $easybashgui;
	else
		export SHELL_LIBRARY_PATH=$1/lib
		source $1/easybashgui;
	fi

}





hello_world_ebg() {

ok_message "Hello world !"
clean_temp
}


init
hello_world_ebg
