#!/usr/bin/env bash




input_ebg() {

input 3 "Your Name" "Igor?" "What do you like" "programing?" "Do you want to finish" "yes?" 2> /dev/null

IFS=$'\n'
array=( $( 0< "${dir_tmp}/${file_tmp}" ) )
IFS=$' \t\n'

local name="${array[0]}";  if_arg_is_an_empty_variable_then_exit "name";
local activity="${array[1]}";  if_arg_is_an_empty_variable_then_exit "activity";
local finish="${array[2]}";  if_arg_is_an_empty_variable_then_exit "finish";

ok_message "Your choice: \n name: $name \n  activity: $activity \n finish: $finish \n"


clean_temp
}



#initialization
supermode="dialog" # possible types of the mode are "yad", "gtkdialog","kdialog", "zenity", "Xdialog", "dialog", and "none" 
cd ../;
source config.sh --init;
cd - >/dev/null


# running menu
input_ebg
