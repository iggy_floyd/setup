#!/usr/bin/env bash



# declare settings array
declare -A settings # please note, that associative array can be created in the bash with version >4.0



ini(){

#initialization
    source `dirname $0`/../utils/utils.sh;

# determine the type of the dialog
    _gui_init "terminal"; # by default: if whiptail and dialog are not installed
    [ `which whiptail` ] && echo "whiptail"  && _gui_init "whiptail"; # if dialog is not installed, use whiptail, otherwise 
    [ `which dialog` ] && echo "whiptail"  && _gui_init "dialog"; # use dialog


}


message() {


 

    cat << _END > _tmp_file_$$

        This is a setup script of the BK-tree package.
        The script properly defines  all environment variables 
        required by the BK-tree.
_END


       _info_box "\"       Hi User! \""  _tmp_file_$$;
       rm _tmp_file_$$;

    

}


cayley_settings() {

# cayley path
       _input_box "CAYLEYPATH" "$HOME/project/bin/bk-tree/cayley/cayley_0.4.0_linux_amd64/";          
        settings[CAYLEYPATH]=$( 0< "$tempfile5" ); rm $tempfile5;

# cayley configpath
       _input_box "CAYLEYCONFIGPATH" "$HOME/project/bin/bk-tree/cayley/configuration/";          
        settings[CAYLEYCONFIGPATH]=$( 0< "$tempfile5" ); rm $tempfile5;

# gremlin api path
       _input_box "GREMLINAPIPATH" "$HOME/project/library/cayley/library/Cayley/JS";          
        settings[GREMLINAPIPATH]=$( 0< "$tempfile5" ); rm $tempfile5;

# gremlin api file
        local api=`dirname $0`/gremlin_api.txt
        _input_box "GREMLINAPI" "$api";          
        settings[GREMLINAPI]=$( 0< "$tempfile5" ); rm $tempfile5;

# cayley Backend Database
           _input_box "CAYLEYBACKEND" "leveldb";   
            settings[CAYLEYBACKEND]=$( 0< "$tempfile5" ); rm $tempfile5;

# cayley Backend Database path
           _input_box "CAYLEYBACKENDPATH" "/tmp/fpsdb";   
            settings[CAYLEYBACKENDPATH]=$( 0< "$tempfile5" ); rm $tempfile5;


# cayley Backend Database initialization
           _input_box "\"CAYLEYBACKENDINI(yes/no)\"" "yes";   
            settings[CAYLEYBACKENDINI]=$( 0< "$tempfile5" ); rm $tempfile5;

# cayley Backend Database configuraton
           _input_box "CAYLEYBACKENCONF" "$CAYLEYCONFIGPATH/cayley_\\\${CAYLEYBACKEND}.cfg";   
            settings[CAYLEYBACKENCONF]=$( 0< "$tempfile5" ); rm $tempfile5;


}


bktree_settings() {


# bk-tree ini file
           _input_box "BKTREEINI" "$HOME/project/application/configs/application.ini";   
            settings[BKTREEINI]=$( 0< "$tempfile5" ); rm $tempfile5;


# prefix of the created files with trees
           _input_box "PREFIXDB" "test_fps_";   
            settings[PREFIXDB]=$( 0< "$tempfile5" ); rm $tempfile5;

# php cayley ini file with a port number
#            _input_box "PHPCAYLEYINI" "$HOME/project/application/modules/Config/Cayley.ini";   
#            settings[PHPCAYLEYINI]=$( 0< "$tempfile5" ); rm $tempfile5;

# port number 
            _input_box "BKTREEPORT" "8888";   
            settings[BKTREEPORT]=$( 0< "$tempfile5" ); rm $tempfile5;

#do-timing
            _radio_list  "BKTREEDOTIMING"  "yes" "no"
            settings[BKTREEDOTIMING]=$( 0< "$tempfile3" );  # either 1 for "yes" or 2 for "no"            
            rm $tempfile3;

# path to bktree executable
 
            local bktreepath=`pwd`/../bin/bktree;
            _input_box "BKTREEPATH" "$bktreepath";   
            settings[BKTREEPATH]=$( 0< "$tempfile5" ); rm $tempfile5;

# here: bk-tree cmd line
            local cmdline_option_bktree="";
            local dotiming;            
            [[ ${settings[BKTREEDOTIMING]} -eq 1 ]] && dotiming="--do-timing";


            settings[BKTREECMD]="${settings[BKTREEPATH]} --prefixDB ${settings[PREFIXDB]} -p ${settings[BKTREEPORT]} $dotiming"

}





print_settings() {

local all_settings="";

    for i in "${!settings[@]}"
        do
            all_settings=`echo "${all_settings}" "$i: ${settings[$i]} 

 "`
        done


    echo "${all_settings}" > _tmp_file_$$;
    _info_box "\"       Your settings: \""  _tmp_file_$$;
    rm _tmp_file_$$;

} 


confirm_correctness(){

    _yes_no_box "\"All is correct? \"";

    return $?;

}


configuring() {

    while true 
    do

        cayley_settings; # 3) set cayley parameters
        bktree_settings; # 4) set bktree parameters
        print_settings;   # 5) to print out all settings
    
        confirm_correctness; # 6) All is correct?
        local correct=$?; # 6) All is correct?
        [[  $correct -eq  0 ]] && break;
    done


}




exporting() {


    export CAYLEYPATH=${settings[CAYLEYPATH]};
    export CAYLEYCONFIGPATH=${settings[CAYLEYCONFIGPATH]};
    export GREMLINAPIPATH=${settings[GREMLINAPIPATH]};
    export GREMLINAPI=${settings[GREMLINAPI]};


    export CAYLEYBACKEND=${settings[CAYLEYBACKEND]};

    # if cayley requires initialization
    [[ "${settings[CAYLEYBACKENDINI]}" == "yes" ]]  && [ `which $CAYLEYPATH/cayley` ] && $CAYLEYPATH/cayley init --db="${settings[CAYLEYBACKEND]}" --dbpath="${settings[CAYLEYBACKENDPATH]}";

    export CAYLEYCMD="$CAYLEYPATH/cayley http --db=${settings[CAYLEYBACKEND]} --dbpath=${settings[CAYLEYBACKENDPATH]}  --assets $CAYLEYPATH -config=$CAYLEYCONFIGPATH/${settings[CAYLEYBACKENCONF]}";
    
    export BKTREEINI=${settings[BKTREEINI]};
    export BKTREECMD=${settings[BKTREECMD]};

}



unsetting() {

    unset CAYLEYPATH CAYLEYCONFIGPATH GREMLINAPIPATH GREMLINAPI CAYLEYCMD BKTREEINI BKTREECMD

}


check_ini_file() {

    [ -f setup_cayley_bktree.ini ] && (

        _yes_no_box "\"Old settings have been found. Should we use them?\"";
        local yes=$?;
        [[  $yes -eq  0 ]] && 
        return 0;
    )



[[ $? -eq 0 ]] &&  return 0;



return 1;

}







# main program goes here


ini; # 1) initialization

message; # 2) greetings

unsetting; # 2.2) clearing all environment variables previously set


check_ini_file; 
if [[ $? -eq 1 ]]  
 then
    configuring; # 3) -- 6) if no previous settings have been found, configuring
 else 
    source setup_cayley_bktree.ini
    rm  setup_cayley_bktree.ini;
fi

  
exporting; # 7) exporting the environment variables

declare -p  settings > setup_cayley_bktree.ini; # 8) save settings

unset  settings # 9) delete settings array

