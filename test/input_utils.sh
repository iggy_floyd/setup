#!/usr/bin/env bash


message() {


echo 	"
	This is an example of use the bash gui in `dirname $0`/../utils/utils.sh:
	
				_gui_init
				_menu
				_radio_list
				_check_list

	"


}


input_utils() {


# make a box to get user input
_input_box "\"Please, provide your name:\""

# read and output the user input
 _info_box "\"Your name is \"" $tempfile5 


# clean up
rm -f $tempfile1 $tempfile2 $tempfile3 $tempfile4 $tempfile5 $tempfile5 $tempfile6


}



message $0

#initialization
source `dirname $0`/../utils/utils.sh;

_gui_init "terminal"; # by default: if whiptail and dialog are not installed
[ `which whiptail` ] &&  _gui_init "whiptail"; # if dialog is not installed, use whiptail, otherwise 
[ `which dialog` ] &&  _gui_init "dialog"; # use dialog




# running the example of gui 
input_utils


